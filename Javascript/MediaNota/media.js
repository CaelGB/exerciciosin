//Precisamos do HTML para receber input do usuário
//Rodar com live server ao invés do code runner

function media(){
    const nota1 = parseFloat(document.getElementById("nota1").value)
    const nota2 = parseFloat(document.getElementById("nota2").value)
    const nota3 = parseFloat(document.getElementById("nota3").value)

    const media = (nota1 + nota2 + nota3)/3

    console.log(`${nota1}, ${nota2}, ${nota3}, ${media}`)

    if(media >= 6){
        window.alert("Aprovado!")
    } else{
        window.alert("Reprovado!")
    }
}