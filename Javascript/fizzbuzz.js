let teste = []

for(i = 0; i < 5; i++){
    let numero = Math.round(Math.random()*101)
    teste.push(numero)
}

for(j of teste){
    if(j % 3 == 0 && j % 5 == 0){
        console.log("FizzBuzz");
    }
    else if(j % 3 == 0){
        console.log("Fizz");
    }
    else if(j % 5 == 0){
        console.log("Buzz");
    }
    else{
        console.log(j);
    }
}